import os
import sys
import re
import requests
import string
import time
from fpdf import FPDF
from datetime import datetime
from bs4 import BeautifulSoup
import shutil
import random


def get_html_contents(base_html):
    contents = ""
    with open(base_html) as cfile:
        contents = cfile.read()
    return contents


def get_page_links(html):    

    marker = 'lstImages.push("' 
    marker_end = '")'
    marker_len = len(marker)

    # get the list of images
    idx = 0
    jpgs = []
    while idx>-1:
        print('finding %s' % idx)
        idx = html.find(marker,idx)
        if idx == -1:
            break
        endidx = 0
        endidx = html.find(marker_end, idx)
        if endidx == -1:
            break
        pagejpg = html[idx+marker_len:endidx]
        print(pagejpg)
        jpgs.append(pagejpg)
        idx = idx+1

    return jpgs


def download_page(page, target_filename):
    num_tries = 20
    while num_tries>0:
        try:
            sleep_time = random.randint(1,10)
            time.sleep(sleep_time)
            print('Trying %s' % page)
            
            response = requests.get(page, stream=True)
            with open(target_filename, 'w') as outfile:
                outfile.write(response.text)
            del response
            break
        except IOError as exc:
            print('Unexpected error- %s' % (exc))
        num_tries = num_tries-1


def download_jpg(jpg, target_filename):
    num_tries = 20
    while num_tries>0:
        try:
            sleep_time = random.randint(1,10)
            time.sleep(sleep_time)
            print('Trying %s' % jpg)
            
            response = requests.get(jpg, stream=True)
            with open(target_filename, 'wb') as outfile:
                shutil.copyfileobj(response.raw, outfile)
            del response
            break
        except:
            print('Unexpected error- %s' % jpg)
            time.sleep(10)
        num_tries = num_tries-1


def download_links_to_jpgs(jpgs):
    jpg_filenames = []
    for i, jpg in enumerate(jpgs):
        jpg_filename = format('c:\\temp\\j%s.jpg' % i)
        download_jpg(jpg, jpg_filename)
        jpg_filenames.append(jpg_filename)
    return jpg_filenames


def save_jpgs_to_pdf(jpg_filenames, target_pdf):
    pdf = FPDF()
    for jpg in jpg_filenames:
        pdf.add_page()
        pdf.image(jpg)
    pdf.output(target_pdf, "F")


def clear_all_tmp_files():
    os.system('del c:\\temp\\*jpg')


def save_jpgs_to_cbz(target_cbz):
    print("saving")
    #cmd = format('zip %s c:\\temp\\*.jpg' % tar get_cbz)
    cmd = format('c:\\PROGRA~1\\7-Zip\\7z.exe a -tzip %s c:\\temp\\*.jpg' % target_cbz)
    os.system(cmd)


def save_one_comic_book(comic_html, target_cbz):
    clear_all_tmp_files()

    contents = get_html_contents(comic_html) 
    jpgs = get_page_links(contents)
    jpg_filenames = download_links_to_jpgs(jpgs)
    save_jpgs_to_cbz(target_cbz)


def parse_issues_urls(comic_base_html):
    urls = []
    contents = get_html_contents(comic_base_html) 
    soup = BeautifulSoup(contents, "html.parser")
    myissues = soup.select('.list a')
    for myissue in myissues:
        issueurl = myissue.attrs['href']       # same as myissue['href']
        print(myissue)
        url = webroot + issueurl
        urls.append(url)
    return urls


def parse_issue_id(url):
    issue_id = ''
    # The-Amazing-Spider-Man-1963/Issue-801?id=135847"       
    idx = url.find('Issue-')
    if idx < 0:
        idx = url.find('Annual-')
    if idx < 0:
        return issue_id

    endidx = url[idx:].find('?id')
    if endidx > 0:
        issue_id = url[idx:endidx+idx]
    return issue_id

    

def save_one_series(comic_base_html, short_code):
    if 'http' in comic_base_html:
        download_page(comic_base_html, "c:\\temp\\base.html")
        comic_base_html = "c:\\temp\\base.html"

    urls = parse_issues_urls(comic_base_html)
    for url in urls:
        issue_id = parse_issue_id(url)
        if len(issue_id)>0:
            target_cbz = short_code + '.' + issue_id + '.cbz'
            target_cbz = 'c:\\temp\\' + target_cbz.lower()
            download_page(url, "c:\\temp\\issue.html")
            url =  "c:\\temp\\issue.html"
            save_one_comic_book(url, target_cbz)


    
#---------------------------------------------------------------
# Prerequsite- 7z software in Windows
#

webroot = 'https://readcomiconline.to'

if len(sys.argv)<2:
    print('comix.py comic_base.html comic_code')
    print('comic_base_html = base url of comic stream')
    print('comic_code      = short code of comic')
    print(' example')
    print(' comix_py https://readcomiconline.to/Comic/The-Amazing-Spider-Man-1963 asm')
    exit(-1)

save_one_series(sys.argv[1], sys.argv[2])


