# COMIX #

Here's the problem.  You're reading your favorite [comic book online](https://readcomiconline.to/)
and the website flashes all kinds of annoying ads.  Or you want to take your comic book reading
offline.  What can you do?

Comix is a python script that downloads one entire series of comics to your local PC for offline
reading.  Just run the script with a selected comic book series, and it downloads all the issues
into [cbz files](https://en.wikipedia.org/wiki/Comic_book_archive).  Use a regular comic book 
reader like [SumatraPDF](https://en.wikipedia.org/wiki/Sumatra_PDF) to read the files on your 
PC- or even on your mobile. 

### Details ###

Readcomiconline.to has a huge repository of both Marvel and DC comics.  Its only downsides are
that its pages has substantial advertisements and annoying visuals.  The best option is to just
scrape the available contents and safely read offline.  

For example, here is the link for all of the [original Amazing Spider-man comics going back 
to 1963](https://readcomiconline.to/Comic/The-Amazing-Spider-Man-1963).

Given this one link, Comix downloads every issue to your local PC for offline reading.

### Usage ###

The comix python script is configured to run in Windows.  Windows uses
the backslash for folders whereas Linux uses the forward slash.  However,
this any user can make this minor change in Linux and still use the script.

To get a series of comics, first browse to [ReadComicOnline](https://readcomiconline.to) and find a comic series to download.  Copy the URL of the series. 

Second, pick a short code to use as a filename prefix for all downloaded
comic book files.  For example,  for the Amazing Spider-man series, I use
the prefix 'asm' and all comic book files get generated with this prefix.

![ASM prefix](./asm.files.png)


### Command line options: ###

    comix.py comic_base.html comic_code

    comic_base_html = base url of comic stream
    comic_code      = short code of comic

    example
    comix_py https://readcomiconline.to/Comic/The-Amazing-Spider-Man-1963 asm


### Screenshots: ###

#### Development environment ####

![Visual Studio 2019](./startup.png)

#### Sample ####

![ASM.800](./asm.800.png)

[AmazingSpiderMan.Issue.800.cbx](./sample/asm.issue-800.cbz)


### What is this repository for? ###

* Download comics book for reading offline
* 0.01

### How do I get set up? ###

* Windows, Visual Studio 2019, Python 3.6
* Dependencies
  * Python packages: BeautifulSoup, requests, fpdf, shutil
  * 7zip 


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

